'use strict';

// Отримання IP-адреси користувача
async function getIPAddress() {
    const response = await fetch('https://api.ipify.org/?format=json');
    const data = await response.json();
    return data.ip;
}

// Отримання інформації про фізичну адресу
async function getAddressInfo(ip) {
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=status,message,continent,country,regionName,city,district`);
    const data = await response.json();
    return data;
}

getIPAddress()
    .then(ip => {
        getAddressInfo(ip)
            .then(addressInfo => {
                console.log('Інформація про адресу:', addressInfo);
            })
            .catch(error => {
                console.error('Помилка отримання інформації про адресу:', error);
            });
    })
    .catch(error => {
        console.error('Помилка отримання IP-адреси:', error);
    });

// Обробка натискання кнопки
document.getElementById('findIP').addEventListener('click', async () => {
    const ip = await getIPAddress();
    const addressInfo = await getAddressInfo(ip);

// Відображення інформації на сторінці
const resultContainer = document.getElementById('resultContainer');
const adressContent = `
	<p><strong>Континент:</strong> ${addressInfo.continent || "Не визначено"}</p>
	<p><strong>Країна:</strong> ${addressInfo.country || "Не визначено"}</p>
	<p><strong>Регіон:</strong> ${addressInfo.regionName || "Не визначено"}</p>
	<p><strong>Місто:</strong> ${addressInfo.city || "Не визначено"}</p>
	<p><strong>Район:</strong> ${addressInfo.district || "Не визначено"}</p>
	`;
    resultContainer.innerHTML = adressContent;
    resultContainer.style.display = "block";

    document.getElementById('findIP').classList.add('disabled');
});
